{% from slspath+"/map.jinja" import consul with context %}

sql_exporter-create-user:
  user.present:
    - name: {{ sql_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

sql_exporter-create-group:
  group.present:
    - name: {{ sql_exporter.service_group }}
    - system: True
    - members:
      - {{ sql_exporter.service_user }}
    - require:
      - user: {{ sql_exporter.service_user }}

sql_exporter-bin-dir:
  file.directory:
   - name: {{ sql_exporter.bin_dir }}
   - makedirs: True

sql_exporter-dist-dir:
 file.directory:
   - name: {{ sql_exporter.dist_dir }}
   - makedirs: True

sql_exporter-config-dir:
  file.directory:
    - name: {{ sql_exporter.config_dir }}
    - makedirs: True

#sql_exporter-required-packages:
#  pkg.installed:
#    - pkgs:
#      - python-pycurl
#      - python-tornado
#    {# Forcing the Salt minion to reload its modules for it to recognize that the proper dependencies are in place to download the sql_exporter #}
#    - reload_modules: True

sql_exporter-install-binary:
  archive.extracted:
    - name: {{ sql_exporter.dist_dir }}/sql_exporter-{{ sql_exporter.version }}
    - source: https://github.com/free/sql_exporter/releases/download/{{ sql_exporter.version }}/sql_exporter-{{ sql_exporter.version }}.{{ grains['kernel'] | lower }}-{{ sql_exporter.arch }}.tar.gz
    - options: --strip-components=1
    - user: root
    - group: root
    - enforce_toplevel: False
#    - require:
#      - pkg: sql_exporter-required-packages
    - unless:
      - '[[ -f {{ sql_exporter.dist_dir }}/sql_exporter-{{ sql_exporter.version }}/sql_exporter ]]'
  file.symlink:
    - name: {{ sql_exporter.bin_dir }}/sql_exporter
    - target: {{ sql_exporter.dist_dir }}/sql_exporter-{{ sql_exporter.version }}/sql_exporter
    - mode: 0755
    - unless:
      - '[[ -f {{ sql_exporter.bin_dir }}/sql_exporter ]] && {{ sql_exporter.bin_dir }}/sql_exporter --version | grep {{ sql_exporter.version }}'

sql_exporter-install-service:
  file.managed:
{% if grains['init'] == 'systemd' %}
    - name: /etc/systemd/system/sql_exporter.service
    - source: salt://sql_exporter/files/sql_exporter.service.j2
{% else %}
    - name: /etc/init.d/sql_exporter
    - source: salt://sql_exporter/files/initscript.j2
    - mode: 755
{% endif %}
    - template: jinja
    - context:
        sql_exporter: {{ sql_exporter }}
    - require:
        - archive: sql_exporter-install-binary
  service.running:
    - name: sql_exporter
    - enable: True
    - start: True
    - require:
      - file: sql_exporter-install-service

sql_exporter-service-reload:
  service.running:
    - name: sql_exporter
    - enable: True
    - watch:
      - file: sql_exporter-install-service
